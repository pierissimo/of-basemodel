'use strict';

function enableSocketIOEvents(Schema, io) {

  Schema.post('save', _save);
  Schema.post('update', _update);
  Schema.post('findOneAndUpdate', _update);
  Schema.post('remove', _remove);
  Schema.post('findOneAndRemove', _remove);

  function _save(doc) {
    io.emit('save', doc);
  }

  function _update(doc) {
    io.emit('update', doc);
  }

  function _remove(doc) {
    io.emit('remove', doc);
  }
}


module.exports = enableSocketIOEvents;

