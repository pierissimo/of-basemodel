'use strict';

var _ = require('lodash');

function parseArgs(_params, _defaultOptions) {
  var args = {};
  _params  = _.isObject(_params) ? _params : {};

  _.forEach(_params, function (v, key) {
    switch (key) {
      case 'rest':
        args.rest = v;
        break;
      case 'events':
        args.events = v;
        break;
      case 'socketIO':
        if (typeof args.socketIO.emit !== 'function') {
          throw new Error('Not a valid SocketIO instance');
        }
        args.socketIO = v;
        break;
    }
  });

  args = _.merge({}, _defaultOptions, args);

  return args;
};


module.exports.parseArgs = parseArgs;