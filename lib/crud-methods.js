'use strict';

var _       = require('lodash');
var Promise = require('bluebird');

function enableCrudMethods(Schema) {

  var _defaultQueryOptions = {
    where: {},
    limit: 100,
    skip: 0,
    sort: {},
    populate: '',
    select: false,
    lean: false
  };

  /**
   * Search resources
   * @param params - Parameters for filtering, sorting, populating, pagination.
   * @param returnQuery = If true, returns the query instead of executing it
   * @returns {Query}
   */
  Schema.statics.$search = function (params, returnQuery) {
    var self  = this;
    var args  = _parse(params);
    var query = this
     .find(args.where)
     .limit(args.limit)
     .skip(args.skip)
     .sort(args.sort)
     .populate(args.populate);

    if (args.select) {
      query = query.select(args.select);
    }

    if (returnQuery) {
      return query;
    }

    return Promise.props({
      count: self.where(args.where).count(),
      items: args.lean ? query.lean().exec() : query.exec()
    });
  };

  /**
   * Search single item
   * @param params - Parameters for filtering, sorting, populating, pagination.
   * @param returnQuery = If true, returns the query instead of executing it
   * @returns {Query}
   */
  Schema.statics.$read = function (params, returnQuery) {
    var query = this.statics.$search(params, returnQuery);

    if (query instanceof Promise) {
      return query.then(function (data) {
        return data.items.pop();
      });
    }

    return query;
  };


  var _parse = function (_params) {
    var args = {};
    _params  = _.isObject(_params) ? _params : {};

    _.forEach(_params, function (v, key) {
      switch (key) {
        case 'where':
          args.where = v;
          break;
        case 'pagesize':
          var limit = parseInt(v);
          if (!isNaN(limit) && limit !== 0) {
            args.limit = limit;
          }
          break;
        case 'page':
          var page = parseInt(v);
          if (!isNaN(page) && page !== 0) {
            args.page = page;
          }
          break;
        case 'sort':
          args.sort = v;
          break;
        case 'populate':
          args.populate = v;
          break;
        case 'q':
          if (_.isString(v)) {
            args.q = _buildFilter(v);
          }
          break;
        case 'select':
          if (!_.isUndefined(v)) {
            args.select = _buildSelectFields(v);
          }
          break;
        case 'lean':
          args.lean = v;
          break
      }
    });

    args.where = _.merge({}, args.where, args.q);
    args       = _.merge({}, _defaultQueryOptions, args);

    if (args.page) {
      args.skip = args.limit * ( args.page - 1);
      delete args.page;
    }

    return args;
  };


  function _buildFilter(q) {
    var filter = {};

    if (q) {
      var re           = new RegExp('.*' + q + '.*', 'i');
      var searchFields = _.filter(Schema.paths, function (f, key) {
        return f.options.searchable;
      });
      filter['$or']    = searchFields.map(function (field) {
        var c         = {};
        c[field.path] = {'$regex': re};
        return c;
      });

      //TODO: investigate a better way to return a 0 query
      if (filter['$or'].length === 0) {
        filter = {'null': 'null'};
      }
    }

    return filter;
  }


  function _buildSelectFields(fields) {
    if (!_.isString(fields) && !_.isObject(fields)) {
      return false;
    }
    if (_.isObject(fields)) {
      _.each(fields, function (f, i) {
        fields[i] = parseInt(f);
      });
    }
    return fields;
  }
}


module.exports = enableCrudMethods;